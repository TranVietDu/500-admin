import { Route } from "react-router-dom"
import User from "../pages/User"
import Question from "../pages/Question"
const Routes = () => {
    return(
        <>
        <Route path='/user' render={() => <User/>}/>
        <Route path='/question' render={() => <Question/>}/>
        </>
    )
}

export default Routes