import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Link, NavLink, Route, Switch } from "react-router-dom";
import Login from "./pages/Login";
import Page from "./pages/Page";
import User from "./pages/User/User";
import Question from "./pages/Question/Question";
import React, { useState } from "react";
import "./index.css";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { Layout, Menu, theme } from "antd";
import Routes from "./Routes/Routes";

const { Header, Sider, Content } = Layout;
function App() {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/login" render={() => <Login />} />
        <Route
          path="/"
          render={() => (
            <Layout>
              <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className="logo" />
                <Menu
                  theme="dark"
                  mode="inline"
                  defaultSelectedKeys={["1"]}
                  items={[
                    {
                      key: "1",
                      icon: (
                        <NavLink to="/user">
                          <UserOutlined />
                        </NavLink>
                      ),
                      label: "User",
                    },
                    {
                      key: "2",
                      icon: 
                          <NavLink to="/question">
                            <VideoCameraOutlined />
                          </NavLink>,
                      label: "nav 2",
                    },
                    {
                      key: "3",
                      icon: <UploadOutlined />,
                      label: "nav 3",
                    },
                  ]}
                />
              </Sider>
              <Layout className="site-layout">
                <Header style={{ padding: 0, background: colorBgContainer }}>
                  {React.createElement(
                    collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                    {
                      className: "trigger",
                      onClick: () => setCollapsed(!collapsed),
                    }
                  )}
                </Header>
                <Content style={{ 
                  margin: "24px 16px",
                  padding: 24,
                  minHeight: 280,
                  background: colorBgContainer,
                 }}>
                    <Route exact path='/user' component={User}/>
                    <Route exact path='/question' component={Question}/>   
                </Content>
              </Layout>
            </Layout>
          )}
        />
        
      </Switch>
    </BrowserRouter>
  );
}

export default App;
