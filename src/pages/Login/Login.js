import styles from './Login.module.scss'

const Login = () => {
  return (
    <div className={styles.login}>
      <div className={styles.overlay}></div>
      <div className={styles.header}>
        
      </div>
      <div className={styles.body}>
        <h1>500 Anh Em</h1>
        <p>LOGIN TO ADMIN DASHBOARD</p>
        <div className={styles.formLogin}>
          <form>
            <input
              type="text"
              placeholder="Username"
              name="email"
            />
            <input
              type="password"
              placeholder="Password"
              name="password"
            />
            <input type="submit" value="LOGIN"/>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
